---
layout: post
title: "FlickrFS果然来了"
date: 2005-11-10
author: erning
comments: false
categories:
---

我曾猜想[会出个FlickrFS][1]，今天果然看到[FlickrFS的Announcement][2]。

三个月前[http://www.google.com/search?q=flickrfs][3]还只能看到一个搜索结果，就是我的那个猜想，今天已经上万条了。

看说明，linux下upload图片更简单了，直接cp到flickrfs的目录下，加tags就是建目录。下载图片也一样，就没有[FlickrBackup][4]什么事情了，rsync一下就好了嘛。

update: 中文支持出问题。试安装[cjkpytho][5]n也不行，修改了一下代码，可以了。可是图片下载却是zero byte，还是等个stable版本再玩。

  [1]: /blog/2005/08/04/flickr-for-fun/
  [2]: http://flickrfs.sourceforge.net/
  [3]: http://www.google.com/search?q=flickrfs
  [4]: http://www.google.com/search?q=flickrbackup
  [5]: http://cjkpython.i18n.org/
