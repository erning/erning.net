---
layout: post
title: "Internationalized Domain Names"
date: 2003-07-18
author: erning
comments: false
categories:
---

My weblog bound with IDN. If you’re using Netscape 7.1/Mozilla 1.4 you’re able to access it by [http://张尔宁.cn][1] (xn--w9s3m31f.cn) or [http://張爾寧.cn][2] (xn--tdtt0fy0x.cn).

More info about IDN: [http://devedge.netscape.com/viewsource/2003/idn/][3]

  [1]: http://xn--w9s3m31f.cn
  [2]: http://xn--tdtt0fy0x.cn
  [3]: http://devedge.netscape.com/viewsource/2003/idn/

