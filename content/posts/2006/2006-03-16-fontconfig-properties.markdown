---
layout: post
title: "fontconfig.properties"
date: 2006-03-16
author: erning
comments: false
categories:
---

如果你在Linux下的jdk1.5程序中文显示有问题(比如Intellij IDEA)，可以把下面的文件复制到”$JAVA_HOME/jre/lib”目录下，文件名是fontconfig.properties。(注意最后一行中文字体的位置)

<!-- more -->

erning@dragon-usb:~/apps/jdk1.5.0_06/jre/lib$ cat fontconfig.properties

    # @(#)linux.fontconfig.properties       1.chinese 03/10/28
    #
    # Copyright 2003 Sun Microsystems, Inc. All rights reserved.
    #
    
    # Version
    
    version=1
    
    # Component Font Mappings
    
    serif.plain.latin-1=-b&h-lucidabright-medium-r-normal--*-%d-*-*-p-*-iso8859-1
    serif.bold.latin-1=-b&h-lucidabright-demibold-r-normal--*-%d-*-*-p-*-iso8859-1
    serif.italic.latin-1=-b&h-lucidabright-medium-i-normal--*-%d-*-*-p-*-iso8859-1
    serif.bolditalic.latin-1=-b&h-lucidabright-demibold-i-normal--*-%d-*-*-p-*-iso8859-1
    
    sansserif.plain.latin-1=-b&h-lucidasans-medium-r-normal-sans-*-%d-*-*-p-*-iso8859-1
    sansserif.bold.latin-1=-b&h-lucidasans-bold-r-normal-sans-*-%d-*-*-p-*-iso8859-1
    sansserif.italic.latin-1=-b&h-lucidasans-medium-i-normal-sans-*-%d-*-*-p-*-iso8859-1
    sansserif.bolditalic.latin-1=-b&h-lucidasans-bold-i-normal-sans-*-%d-*-*-p-*-iso8859-1
    
    monospaced.plain.latin-1=-b&h-lucidatypewriter-medium-r-normal-sans-*-%d-*-*-m-*-iso8859-1
    monospaced.bold.latin-1=-b&h-lucidatypewriter-bold-r-normal-sans-*-%d-*-*-m-*-iso8859-1
    monospaced.italic.latin-1=-b&h-lucidatypewriter-medium-i-normal-sans-*-%d-*-*-m-*-iso8859-1
    monospaced.bolditalic.latin-1=-b&h-lucidatypewriter-bold-i-normal-sans-*-%d-*-*-m-*-iso8859-1
    
    dialog.plain.latin-1=-b&h-lucidasans-medium-r-normal-sans-*-%d-*-*-p-*-iso8859-1
    dialog.bold.latin-1=-b&h-lucidasans-bold-r-normal-sans-*-%d-*-*-p-*-iso8859-1
    dialog.italic.latin-1=-b&h-lucidasans-medium-i-normal-sans-*-%d-*-*-p-*-iso8859-1
    dialog.bolditalic.latin-1=-b&h-lucidasans-bold-i-normal-sans-*-%d-*-*-p-*-iso8859-1
    
    dialoginput.plain.latin-1=-b&h-lucidatypewriter-medium-r-normal-sans-*-%d-*-*-m-*-iso8859-1
    dialoginput.bold.latin-1=-b&h-lucidatypewriter-bold-r-normal-sans-*-%d-*-*-m-*-iso8859-1
    dialoginput.italic.latin-1=-b&h-lucidatypewriter-medium-i-normal-sans-*-%d-*-*-m-*-iso8859-1
    dialoginput.bolditalic.latin-1=-b&h-lucidatypewriter-bold-i-normal-sans-*-%d-*-*-m-*-iso8859-1
    
    # chinese
    
    serif.plain.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    serif.italic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    serif.bold.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    serif.bolditalic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    
    sansserif.plain.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    sansserif.italic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    sansserif.bold.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    sansserif.bolditalic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    
    monospaced.plain.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    monospaced.italic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    monospaced.bold.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    monospaced.bolditalic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    
    dialog.plain.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    dialog.italic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    dialog.bold.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    dialog.bolditalic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    
    dialoginput.plain.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    dialoginput.italic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    dialoginput.bold.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    dialoginput.bolditalic.chinese=-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1
    
    # Search Sequences
    
    sequence.allfonts=latin-1,chinese
    
    # Exclusion Ranges
    
    # Font File Names
    
    filename.-misc-simsun-medium-r-normal--*-%d-*-*-c-*-iso10646-1=/usr/share/fonts/Windows/simsun.ttc
    
