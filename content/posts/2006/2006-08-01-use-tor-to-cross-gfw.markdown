---
layout: post
title: "穿墙术"
date: 2006-08-01
author: erning
comments: false
categories:
---

[{% img left http://static.flickr.com/71/169268189_1e0cd59dec_o.jpg %}][3]

劳山道士的穿墙术是假货，但是现在也许你也不得不找一些方法来钻墙了。tor正是个好工具。

在gentoo linux下安装tor相当简单

    # emerge tor

然后在/etc/tor/目录下创建配置torrc和tor-tsocks.conf。可以参考torrc.sample。

使用前记得启动一下(/etc/init.d/tor start)，或者加入自动启动(rc-update add tor default)。有些时候你可能还需要手工重新启动tor(/etc/init.d/tor restart)。

使用的时候可以在命令前加tsocks，如tsocks opera。这样opera就可以钻墙了。
如果使用firefox，那么它有一个非常方便的插件[FoxyProxy][2]。

  [1]: http://tor.eff.org/
  [2]: http://foxyproxy.mozdev.org/
  [3]: http://www.flickr.com/photos/zendragon/169268189/
