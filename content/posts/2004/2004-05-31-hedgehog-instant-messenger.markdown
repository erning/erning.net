---
layout: post
title: "Hedgehog Instant Messenger"
date: 2004-05-31
author: erning
comments: false
categories:
---

即时通讯的项目开始进行了，是基于[jabber协议][5]的。先给个原始版本的截图

Windows XP  
[![Windows XP][1]][3]

GTK-2  
[![GTK-2][2]][4]


目前客户端运行在Win32下，图形界面用[wxWidgets][6]框架编写，希望能够容易移植到GTK2下。项目还在早期开发状态，有很多子项目需要完成，欢迎有兴趣的朋友参与。

  * Jabber协议的实现
  * 图形界面控件
  * Plugin接口
  * wxSSL http://wxssl.sourceforge.net
  * 服务端

源代码使用[subversion][7]控制版本，暂存在svn.draognsoft.net

  [1]: http://farm1.staticflickr.com/14/19431580_2003c330fc_o.png
  [2]: http://farm1.staticflickr.com/16/19431581_e19790fb42_o.png
  [3]: http://www.flickr.com/photos/zendragon/19431580/
  [4]: http://www.flickr.com/photos/zendragon/19431581/
  [5]: http://www.jabber.org/
  [6]: http://wxwindows.sourceforge.net/
  [7]: http://subversion.tigris.org/
