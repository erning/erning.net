---
layout: post
title: "Fedora Core 2下的中文"
date: 2004-05-19
author: erning
comments: false
categories:
---

今天安装了Fedora Core 2，测试了一下[《在Fedora的英文界面下使用中文》][1]，一次安装成功。

只是/etc/X11/xinit/xinitrc.d/xinput和/etc/gtk-2.0/gtk.immodules两个文件不能按照原文的patch，需要在相应位置修改一下。

安装测试是在vmware 4.5.1下进行。

  [1]: /blog/2004/01/05/chinese-under-english-locale/
