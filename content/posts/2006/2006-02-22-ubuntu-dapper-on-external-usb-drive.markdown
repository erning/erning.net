---
layout: post
title: "Ubuntu Dapper on External USB Drive"
date: 2006-02-22
author: erning
comments: false
categories:
---

在Ubuntu 6.0.4 (Dapper Drake)下，中文粗体已经不需要额外打补丁了。OpenOffice的中文显示也正常。

[![][1]][3]

另外，将linux安装在usb硬盘上也是很有趣的一件事

[![][2]][4]

相关文档

  * [Breezy loaded on external USB drive !][5]
  * [Upgrade to Dapper][6]
  * [Free NTFS read/write filesystem for Linux][7]

中文字体的配置文件 /etc/fonts/local.conf

    <fontconfig>
        <match target="font">
            <!-- check to see if the font is just regular -->
            <test name="weight" compare="less_eq">
                <int>100</int>
            </test>
            <!-- check to see if the pattern requests bold -->
            <test target="pattern" name="weight" compare="more_eq">
                <int>180</int>
            </test>
            <!-- set the embolden flag -->
            <edit name="embolden" mode="assign">
                <bool>true</bool>
            </edit>
        </match>
        <match target="font" >
            <test name="family" qual="any" >
                <string>SimSun</string>
                <string>SimHei</string>
                <string>NSimSun</string>
                <string>MingLiU</string>
                <string>宋体</string>
                <string>黑体</string>
            </test>
            <test name="pixelsize" compare="more">
                <double>11</double>
            </test>
            <test name="pixelsize" compare="less">
                <double>16</double>
            </test>
            <edit mode="assign" name="antialias" >
                <bool>false</bool>
            </edit>
            <edit name="globaladvance" mode="assign">
                <bool>false</bool>
            </edit>
        </match>
    </fontconfig>

  [1]: http://static.flickr.com/33/103018523_6863a3ca5f_m.jpg
  [2]: http://static.flickr.com/35/103016520_383c95374b_m.jpg
  [3]: http://www.flickr.com/photos/zendragon/103018523/
  [4]: http://www.flickr.com/photos/zendragon/103016520/
  [5]: http://ubuntuforums.org/showthread.php?t=80811
  [6]: http://ubuntuforums.org/showthread.php?t=87262
  [7]: http://www.jankratochvil.net/project/captive/
