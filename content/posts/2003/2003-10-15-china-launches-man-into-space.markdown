---
layout: post
title: "China launches man into space"
date: 2003-10-15
author: erning
comments: false
categories:
---

As a Chinese, I’m very proud today.

[![Wan Hu][1]][2]  
Ming Dynasty astronaut – Wan Hu

[![Yang Liwei][3]][4]  
Today – Yang Liwei

  [1]: https://live.staticflickr.com/2780/4240928539_9ef2e717d9.jpg
  [2]: https://www.flickr.com/photos/zendragon/4240928539/
  [3]: https://live.staticflickr.com/2641/4241700226_180b167ecb_m.jpg
  [4]: https://www.flickr.com/photos/zendragon/4241700226/

