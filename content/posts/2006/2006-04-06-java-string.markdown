---
layout: post
title: "Java String"
date: 2006-04-06
author: erning
comments: false
categories:
---

挺无聊的题目真的难倒不少人，

```java
public class Hello {
    public static void main(String[] args) {
        String s = "123";
        System.out.println(s);
        m1(s);
        System.out.println(s);
 
        int i = 123;
        System.out.println(i);
        m2(i);
        System.out.println(i);
    }
 
    private static void m1(String s) {
        s = "321";
    }
 
    private static void m2(int i) {
        i = 321;
    }
}
```

运行结果是什么？
