---
layout: post
title: "上苍保佑吃完了饭的人民"
date: 2006-02-24
author: erning
comments: false
categories:
---

吃完的饭有些兴奋，上网转转看一下wordpress里关于theme编写的文档，结果[http://codex.wordpress.org/][1]无法访问。操！虽然后来找到[http://www.tamba2.org.uk/wordpress/docs/][2]可以将就一下，但是已经不兴奋了。

听听歌吧，今天的曲目是《社会主义好》、《我们走在大路上》、《社员都是向阳花》、《红旗下的蛋》、《上苍保佑吃完了饭的人民》…… 之后心情舒畅多了。你[也想听听][3]？

{% blockquote %}
真的不敢想要能够活着升天  
只要能够活下去正确地浪费剩下的时间  
这要经验还要时间  
眼泪眼屎意守丹田  
我们也只能这样忍受  

……  

请上苍来保佑这些随时可以出卖自己  
随时准备感动绝不想死也不知所终  
开始感觉到撑的人民吧  
{% endblockquote %}

  [1]: http://codex.wordpress.org/
  [2]: http://www.tamba2.org.uk/wordpress/docs/
  [3]: http://www.google.com/search?q=%E7%BA%A2%E8%89%B2%E6%91%87%E6%BB%9A
