---
layout: post
title: "以社区开发的形式来做公司项目"
date: 2004-05-02
author: erning
comments: false
categories:
---

看到这个[消息][1]，Microsoft也在SourceForge上发布了一个项目[wix][2]。

想想我们也可以在公司里弄一个项目，以这种形式来试试。最后先找了个[Instant Message][3]的项目，放在SourceForge上。有兴趣的朋友可以来参加。

其实我们公司已经有先例了，当时做的个URL转发的[mod_forward][5]，就发布在SourceForge上。只是那个项目太小，仅我一个成员。

  [1]: http://blogs.msdn.com/robmen/archive/2004/04/05/107709.aspx
  [2]: http://sourceforge.net/projects/wix
  [3]: http://sourceforge.net/projects/five/
  [4]: http://sourceforge.net/projects/myforward/
