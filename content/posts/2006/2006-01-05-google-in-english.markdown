---
layout: post
title: "Google in English"
date: 2006-01-05
author: erning
comments: false
categories:
---

现在Google中文版本少了一个’Google in English’的链接。实际上那个链接是[http://www.google.com/ncr][1]，点完这个后，在大陆访问Google缺省显示界面变为英文。

或者你用Firefox而且有extension Cookie Editor，修改浏览器的cookie也可以达到这个目的。.google.com domain下的PREF，将CR=?改为CR=2

  [1]: http://www.google.com/ncr
