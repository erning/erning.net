---
layout: post
title: "Gentoo Linux Dailup Internet by CDMA Phone"
date: 2005-11-18
author: erning
comments: false
categories:
---

My CDMA phone connects to the laptop via a USB cable. So I have to configurate the kernel with USB Modem support.

    Device Drivers -->
        USB support -->
            [M] Support for Host-side USB
            [M] USB Modem (CDC ACM) support

Check if the device is well reconized after install and modprobe the module(cdc_acm).

    # dmesg | grep acm
    usbcore: registered new driver cdc_acm
    drivers/usb/class/cdc-acm.c: v0.23:USB Abstract Control Model driver for USB modems and ISDN adapters

Great, the device has been recognized. Then create the ttyACM0 node in /dev is it does exist.

    # mknod /dev/ttyACM0 c 166 0

Edit the configuration file /etc/conf.d/net.ppp0 (or .ppp1, .ppp2). Here’s mine

**/etc/init.d/net.ppp0:**

    PEER="cdma"                     # Define peer (aka ISP)
    DEBUG="yes"                     # Turn on debugging
    PERSIST="no"                    # Redial after being dropped
    ONDEMAND="no"                   # Only bring the interface up on demand?
    MODEMPORT="/dev/ttyACM0"        # TTY device modem is connected to
    LINESPEED="230400"              # Speed pppd should try to connect at
    INITSTRING=""                   # Extra init string for the modem
    DEFROUTE="yes"                  # Must pppd set the default route?
    HARDFLOWCTL="yes"               # Use hardware flow control?
    ESCAPECHARS="yes"               # Use escape caracters ?
    PPPOPTIONS=""                   # Extra options for pppd
    USERNAME="card"                 # The PAP/CHAP username
    PASSWORD="card"                 # Your password/secret.  Ugly I know, but i
                                    # will work on something more secure later
                                    # on.  700 permission on /etc/init.d/net.ppp0
                                    # should be enouth for now.
    NUMBER="#777"                   # The telephone number of your ISP
                                    # leave blank for leased-line operation.
    REMIP=""                        # The ip of the remote box if it should be set
    NETMASK=""                      # Netmask
    IPADDR=""                       # Our IP if we have a static one
    MRU="768"                       # Sets the MRU
    MTU="768"                       # Sets the MTU
    RETRYTIMEOUT="60"               # Retry timeout for when ONDEMAND="yes" or
                                    # PERSIST="yes"
    IDLETIMEOUT="600"               # Idle timeout for when ONDEMAND="yes"
    PEERDNS="yes"                   # Should pppd set the peer dns?

    AUTOCFGFILES="no"               # By default this scripts will generate
                                    # /etc/ppp/chat-isp, /etc/ppp/chap-secrets,
                                    # /etc/ppp/pap-secrets and /etc/ppp/peers/isp
                                    # automatically.  Set to "no" if you experience
                                    # problems, or need specialized scripts.  You
                                    # will have to create these files by hand then.

    AUTOCHATSCRIPT="no"             # By default this script iwll generate
                                    # /etc/ppp/chat-${PEER} automatically. Set to "no"
                                    # if you experience problems, or need specialized
                                    # scripts. You will have to create these files by
                                    # hand then.

    # Directory where the templates is stored
    TEMPLATEDIR=/etc/ppp
    Then have to manual edit two additional files /etc/ppp/chat-cdma and /etc/ppp/peers/cdma.

**/etc/ppp/chat-cdma:**

    'ABORT' 'BUSY'
    'ABORT' 'ERROR'
    'ABORT' 'NO ANSWER'
    'ABORT' 'NO CARRIER'
    'ABORT' 'NO DIALTONE'
    'ABORT' 'Invalid Login'
    'ABORT' 'Login incorrect'
    '' 'ATZ'
    'OK' 'ATDT#777'
    'CONNECT' ''
    'TIMEOUT' '5'
    '~--' ''

**/etc/ppp/peers/cdma:**

    connect "/usr/sbin/chat -v -f /etc/ppp/chat-cdma"
    defaultroute
    usepeerdns
    /dev/ttyACM0
    230400
    local
    novj

Now everything is OK. To dialup the Internet simply type

    /etc/init.d/net.ppp0 start

The CDMA ISP is China Unicom.

If the above configurations does not fit for your situation [try to search more information here][2].

  [1]: http://www.chinaunicom.com.cn/
  [2]: http://www.google.com/search?hl=en&lr=&newwindow=1&q=linux+usb+modem+cdma+dialup&btnG=Search
