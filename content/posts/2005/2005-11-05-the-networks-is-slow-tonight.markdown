---
layout: post
title: "今晚酒店的网络拥堵"
date: 2005-11-05
author: erning
comments: false
categories:
---

酒店免费上网，不过今天来的blogger很多，导致网络拥堵。。。

    dragon ~ # traceroute -n blog.cnblog.org
    traceroute to blog.cnblog.org (211.152.33.116), 30 hops max, 40 byte packets
     1  192.168.50.254  0.238 ms  0.137 ms  0.140 ms
     2  218.1.60.202  1112.622 ms  987.631 ms  1048.243 ms
     3  218.1.62.57  873.728 ms  782.866 ms  1187.260 ms
     4  218.1.4.41  948.176 ms  1076.316 ms  1000.450 ms
     5  218.1.0.214  1208.034 ms  944.970 ms  976.052 ms
     6  61.152.81.66  1090.601 ms  1033.477 ms  959.953 ms
     7  61.152.87.162  1522.018 ms  1575.237 ms  1390.375 ms
     8  211.152.63.57  937.147 ms  1070.089 ms  1265.299 ms
     9  211.152.63.30  1518.643 ms  1285.596 ms  1388.218 ms
    10  211.152.33.116  1515.401 ms  1259.340 ms  962.128 ms
    dragon ~ #

明天会场的wifi会不会更挤？
