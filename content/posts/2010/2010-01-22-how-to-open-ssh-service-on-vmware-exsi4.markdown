---
layout: post
title: "HOW-TO: ESXi and SSH"
date: 2010-01-22
author: erning
comments: false
categories:
---

每次ESXi要在服务器上开启SSH服务，总是忘记具体操作方法，都要去google才行，这里记录一下。

{% blockquote %}
    1. Go to the ESXi console and press alt+F1
    2. Type: unsupported
    3. Enter the root password(No prompt, typing is blindly)
    4. At the prompt type "vi /etc/inetd.conf"
    5. Look for the line that starts with "#ssh" (you can search with pressing "/")
    6. Remove the "#" (press the "x" if the cursor is on the character)
    7. Save "/etc/inetd.conf" by typing ":wq!"
    8. Restart the management service "/sbin/services.sh restart"
{% endblockquote %}

用下面的方法可以避免重新启动机器

{% blockquote %}
    8. Restart the management service "/sbin/services.sh restart"
    10 – Kill inetd : kill `ps | grep inetd | cut -f2 -d" "`
    11 – Start inetd: inetd
{% endblockquote %}

参考：[VMWare KB: Tech Support Mode for Emergency Support][1]

[1]: http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1003677
