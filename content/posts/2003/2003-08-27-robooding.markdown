---
layout: post
title: "Robocoding"
date: 2003-08-27
author: erning
comments: false
categories:
---

Robocode is really a cool game for developers. It cost me two weeks to build the first fight-able bot. So no time to blog :p  
Click [cvsweb][1] to access the source.

More about robocode:

  * [http://www.robocode.net][2]
  * [http://www.robochina.org][3]
  * [http://robocoderepository.com][4]

 [1]: http://cvs.dragonsoft.net/horde/chora/cvs.php/robocoding/bots/
 [2]: http://www.robocode.net/
 [3]: http://www.robochina.org/
 [4]: http://robocoderepository.com/
