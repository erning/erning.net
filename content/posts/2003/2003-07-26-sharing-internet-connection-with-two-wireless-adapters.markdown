---
layout: post
title: "Sharing Internet Connection with Two Wireless Adapters"
date: 2003-07-26
author: erning
comments: false
categories:
---

My brother comes to Xiamen for a few days. His laptop and my desktop have to share the single Internet connection. The laptop has a combo wireless adapter and mine has a usb wireless adapter. There’s no access point at my house. We tried to use ad-hoc mode.

First I tried to use windows 2000 as the gateway. The connection between the laptop and the desktop is ok. But the laptop was fail to connect to the Internet either using NAT nor routing directly. The laptop had to surf the web through a proxy.

Switched my destop to linux environment. Simply installed the usb apapter’s driver and enable ip forwarding. The laptop was able to connect to the Internet.

  * The usb wireless adapter is Linksys [WUSB-11 v2.6][1].
  * The driver for linux is [Atmel AT76C503A-based Wireless Devices][2].

  [1]: http://www.linksys.com/products/product.asp?grid=22&prid=435
  [2]: http://atmelwlandriver.sourceforge.net/
