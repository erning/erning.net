---
layout: post
title: "there was no one left to speak out"
date: 2010-12-02
author: erning
comments: false
categories:
---

> Als die Nazis die Kommunisten holten,  
> habe ich geschwiegen;  
> ich war ja kein Kommunist.  
> 
> Als sie die Sozialdemokraten einsperrten,  
> habe ich geschwiegen;  
> ich war ja kein Sozialdemokrat.
> 
> Als sie die Gewerkschafter holten,  
> habe ich nicht protestiert;  
> ich war ja kein Gewerkschafter.
> 
> Als sie die Juden holten,  
> habe ich geschwiegen;  
> ich war ja kein Jude.
>
> **Martin Niemöller** -- [wikiquote](http://en.wikiquote.org/wiki/Martin_Niem%C3%B6ller)
