---
layout: post
title: "Filter spam and virus"
date: 2003-10-28
author: erning
comments: false
categories:
---

I’m currently working on a large email system which will serives millions of mailboxes – like hotmail.com.

Except sending/receiving emails, the most important feature is spam and virus filtering. I’m using [SpamBayes][1] for spam filtering and [AnomySanitizer][2] plus [ClamAV][3] for virus filtering. They look working well.

Here’s a [Chinese document][4] about how to configurate SpamBayes, AnomySanitizer and ClamAV on an exists email system.

The next problem is to make the email system effective and scalable. Anyone has suggestion?

  [1]: http://spambayes.sourceforge.net/
  [2]: http://mailtools.anomy.net/
  [3]: http://clamav.sourceforge.net/
  [4]: http://www.dragonsoft.net/cgi-bin/moin.cgi/_e5_9c_a8_e5_a4_a9_e5_a5_87_e9_82_ae_e4_bb_b6_e7_b3_bb_e7_bb_9f_e4_b8_8a_e5_8a_a0_e8_a3_85_e5_9e_83_e5_9c_be_e9_82_ae_e4_bb_b6_e5_92_8c_e7_97_85_e6_af_92_e8_bf_87_e6_bb_a4_e5_99_a8
