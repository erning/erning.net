---
layout: post
title: "Firefox Configuration"
date: 2006-07-14
author: erning
comments: false
categories:
---

**Generated:** Fri Jul 14 2006 07:40:33 GMT+0800 (CST)  
**User Agent:** Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060710 Firefox/1.5.0.4  
**Build ID:** 2006071015  

**Enabled Extensions**: (19)

  * Adblock 0.5.3.043
  * Adblock Filterset.G Updater 0.3.0.4
  * Add N Edit Cookies 0.2.1.0
  * Bookmarks Synchronizer 3 1.0.2
  * ColorZilla 0.8.2
  * CuteMenus – Crystal SVG 1.2
  * DOM Inspector 1.8.0.4
  * FlashGot 0.5.9.995
  * FoxyProxy 1.0
  * Greasemonkey 0.6.4
  * Header Monitor 0.3.4
  * Html Validator 0.7.9.3
  * Live HTTP Headers 0.12
  * MeasureIt 0.3.5
  * Mouse Gestures 1.5
  * MR Tech Local Install 5.2
  * NoScript 1.1.4.1
  * Tab Mix Plus 0.3.0.5
  * Viagra 0.2.0.1
  * Web Developer 1.0.2

**Installed Themes:** (1)

  * Firefox (default)

**Installed Plugins:** (6)

  * Google VLC multimedia plugin 1.0
  * mplayerplug-in 3.21
  * QuickTime Plug-in 6.0
  * RealPlayer 9
  * Shockwave Flash
  * Windows Media Player Plugin

