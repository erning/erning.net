---
layout: post
title: "My Linux Gateway"
date: 2010-01-05
author: erning
comments: false
categories:
---

[![示意图][1]][2]

使用透明网关，将web流量通过squid走，cache之后可以提高家里机器访问web的速度。

通过squid配置，将被墙的站点设置成通过SSH隧道走。但squid本身不能forward给socks5代理，因此中间加了个privoxy中转。被墙的站点也可以在squid缓存，节省流量。

如果所有的web访问都通过SSH隧道，访问国内的站点慢了。不仅会反向被墙还浪费海外主机的流量。

没有海外主机的时候，SSH隧道可以用TOR替代。

https不可以做透明代理，这时候需要将浏览器的proxy设置到网关的squid。

图上没标出，网关上还安装有bind，提供域名解析服务，用来对付DNS解析被篡改的问题。

现在这个硬件用的是[Asus EeeBox b202][3]，安静省电。

  [1]: http://farm3.static.flickr.com/2737/4192451789_ec84d67d3f.jpg
  [2]: http://www.flickr.com/photos/zendragon/4192451789/
  [3]: http://www.asus.com.cn/product.aspx?P_ID=QUObl5lSRQQ3lSqJ

