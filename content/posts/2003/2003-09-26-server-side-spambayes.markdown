---
layout: post
title: "Server Side Spambayes II"
date: 2003-09-26T13:00:00
author: erning
comments: false
categories:
---

Since everyone has a different idea of what is spam. The spam filter with a global database will filter some useful emails. It’s not acceptable for customers although there’re only 1% useful emails filtered as spam. So we individual spam databases for each mailbox and let the customers to train the spam filter themselves.

Three buttons was added to the web mail. ‘Delete as Spam’, ‘Recover from Spam’ and ‘Reset Spam Filter’. It’s similar to SpamBayes plug-in for Outlook.

I’ve configured the solution on a production server. Below is the summary of the e-mail server’s log summary for Sep. 25,

    Grand Totals
    ------------
    messages

      87812   received
      56351   delivered
       2012   forwarded
        606   deferred  (5096  deferrals)
       1342   bounced
      54150   rejected (49%)
          0   reject warnings
          0   held
          0   discarded (0%)

       3486m  bytes received
       3816m  bytes delivered
      13591   senders
       4993   sending hosts/domains
       6937   recipients
       1750   recipient hosts/domains

