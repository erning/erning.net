---
layout: post
title: "Chinese under English Locale"
date: 2004-01-05
author: erning
comments: false
categories:
---

The user interface of Fedora Core 1 under English locale is quite pretty but the Chinese one still ugly. Does it support reading and writing Chinese under English locale like Windows 2K? This article shows how I installed Chinese support for Fedora under English locale(en_US.UTF-8).

### Fedora Installation

Select only English(USA) locale. Some fonts and input methods will also installed if you select Chinese locale. But those are no use.

Select and Install Gnome, Development, Kernel Development, Gnome Development and X Development. You may ignore the development packages if you don’t need them at all.

### Fonts Installation

Copy the TrueType fonts you want from Microsoft Windows to /usr/share/fonts/windows/. You many find the fonts at Windows’ \WINNT\Fonts directory.

Than execute the commands as root.

    cd /usr/share/fonts/windows
    ttmkfdir .
    cp fonts.scale fonts.dir
    chkfontpath --add /usr/share/fonts/windows

Here’s the fonts I installed

  * (Arial) - arial.ttf arialbd.ttf arialbi.ttf ariali.ttf
  * (Courier New) - cour.ttf courbi.ttf courbd.ttf couri.ttf
  * (Tahoma) - tahoma.ttf tahomabd.ttf
  * (Verdana) - verdana.ttf verdanab.ttf verdanai.ttf verdanaz.ttf
  * (GB) - simsun.ttc
  * (BIG5) - mingliu.ttc
  * (KR) - batang.ttc gulim.ttc
  * (JP) - msgothic.ttc msmincho.ttc

### Input Method Installation and Configuration

The Smart Common Input Method platform, SCIM for short, is an input method that works under English locale. You may download it from its website http://www.turbolinux.com.cn/~suzhe/scim/. I just installed the following two RPM packages

    rpm -hUv scim-0.8.2-1.i586.rpm
    rpm -hUv scim-chinese-0.2.6-1.i586.rpm

Than, you have to midify the file /etc/X11/xinit/xinitrc.d/xinput, in order to start SCIM automatically after X started. Here’s the patch file

    115c115,118
    < XIM="none"
    ---
    > # XIM="none"
    > if [ -e /usr/bin/scim ]; then
    > XIM="SCIM"
    > fi
    174a178,180
    > SCIM)
    > XIM_PROGRAM=scim
    > XIM_ARGS="-d" ;;

If you want, also change zh_CN*), zh_TW*), etc., to use SCIM under those locale.

And you need modify the file /etc/gtk-2.0/gtk.immodules, to use X input methods in gtk applications. Here’s the patch file

    37c37
    < "xim" "X Input Method" "gtk20" "/usr/share/locale" "ko:ja:zh"
    ---
    > "xim" "X Input Method" "gtk20" "/usr/share/locale" "en:ko:ja:zh"

Also comment the IMs that you never need.

###Done

Restart X.
