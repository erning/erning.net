---
layout: post
title: "工程师普遍采用的时区是当地时区-8"
date: 2010-02-27
author: erning
comments: false
categories:
---

[![苹果往事][2]][1]

我很喜欢[《苹果往事》][1]这本书，它以工程师的视角写记录了很多趣闻轶事。加上是[APPLE \]\[][3]将我带入计算机这个领域的，所以我对这家公司有着相当的感情。

书中有多很有趣的事，放到近三十年后的今天也还一样。例如，工程师周末、晚上常加班，还是主动的；不少有价值的东西是工程师利用业余时间制作的；用代码行数统计工程师的工作量时工程师会填上负数；开发经理和工程师互动常有困难；工程师有惰性时，好的老板能够逼工程师完成工程师自己认为不可能的任务；现实扭曲力场(Reality Distortion Field)对创造力的作用….

还有一点我看出来了，工程师普遍采用的时区是当地时区-8 

书中的故事都可以着[folklore.org][4]网站上读到。

  [1]: http://www.douban.com/subject/4214837/
  [2]: http://img2.douban.com/lpic/s4114974.jpg
  [3]: http://en.wikipedia.org/wiki/Apple_II_series
  [4]: http://www.folklore.org/
