---
layout: post
title: "安居客移动应用"
date: 2010-11-20
author: erning
comments: false
categories:
---

iPhone应用真是太火了，我们一位同事利用[安居客][1]的API开发了一个查看周边房屋价格走势的iPhone应用，看上去还挺不错的，因此我们把它给转正了。经过调整用户界面，重构代码，然后重新以公司的帐号发布。

<del>这个正式的app正在苹果的审核中，很快就会出现在itunes上了。这里先给大家透露一下它的界面。</del>  
更新：itune上的正式的app

[![5189766807][7]][3] [![5189766461][8]][4] [![5189766191][9]][5]

好了，下面说正题。这篇Blog的重点是**招聘iPhone和Andriod的开发工程师**。只要你对移动开发有很高的热情和很强的学习能力，不论你喜欢iOS还是Andriod平台我们都欢迎。如果你对移动开发已经很熟练了，或者在appstore上已经发布过应用，我们也是欢迎的。

关于安居客，可以参考年初的[这篇blog][10]。当然，里面的各项数据已经out了，简单的x4应该和目前的差不多。

如果你有兴趣，请尽快与我联系。另外，之前招聘的职位长期有效。

 [1]: http://www.anjuke.com/
 [2]: http://itunes.apple.com/app/id394069765?mt=8
 [3]: http://www.flickr.com/photos/zendragon/5189766807/
 [4]: http://www.flickr.com/photos/zendragon/5189766461/
 [5]: http://www.flickr.com/photos/zendragon/5189766191/
 [7]: http://farm5.static.flickr.com/4153/5189766807_e901d76431_m.jpg
 [8]: http://farm2.static.flickr.com/1273/5189766461_3945bcfe7b_m.jpg
 [9]: http://farm5.static.flickr.com/4125/5189766191_1b9b999542_m.jpg
 [10]: /blog/archives/2010/01/21/anjuke-is-recruiting-architect
