---
layout: post
title: "Tagging First Chatable Messenger"
date: 2004-06-25
author: erning
comments: false
categories:
---

用了近一个月时间做jabber协议的类封装，再加上一个很简单的图形界面，第一个可以文本聊天的即时通讯客户端出来了。

我想有这个基础应该可以提交给公司，招聘更多的人来继续这个项目了。

源代码和可执行文件下载

  * [hedgehog-20040625-src.tag.bz2][1]
  * [hedgehog-20040625-lib.tag.bz2][2]
  * [hedgehog-20040625-dll.tag.bz2][3]
  * [hedgehog-20040625-bin.tag.bz2][4]

通过下面的命令可以获取最新的源代码

    svn co svn://svn.dragonsoft.net/hedgehog/trunk

  [1]: http://prdownloads.sourceforge.net/five/hedgehog-20040625-src.tag.bz2?download
  [2]: http://prdownloads.sourceforge.net/five/hedgehog-20040625-lib.tag.bz2?download
  [3]: http://prdownloads.sourceforge.net/five/hedgehog-20040625-dll.tag.bz2?download
  [4]: http://prdownloads.sourceforge.net/five/hedgehog-20040625-bin.tag.bz2?download
