---
layout: post
title: "Server Side Spambayes"
date: 2003-09-21
author: erning
comments: false
categories:
---

Installed and tested Spambayes on one of the production email server of our company. It works very well. About 30,000 (around 1GByte) junk emails are filtered per day.

The email server hosts more than 2,500 domains and 30,000 mailboxes.

Currently, we use global spam recognition database and only drop the very certain junk emails. I think it’s better to setup database for each mailbox and provide an user friendly control panel.
