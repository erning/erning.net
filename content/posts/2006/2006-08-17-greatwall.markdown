---
layout: post
title: "长城"
date: 2006-08-17
author: erning
comments: false
categories:
---

蛮不错的歌词

{% blockquote %}
词:刘卓辉  
曲:黄家驹  
主唱:黄家驹  

遥远的东方  
辽阔的边疆  
还有远古的破墙  
前世的沧桑  
后世的风光  
万里千山牢牢接壤  
围着老去的国度  
围着事实的真相  
围着浩瀚的岁月  
围着欲望与理想  
迷信的村庄  
神秘的中央  
还有昨天的战场  
皇帝的新衣  
热血的樱枪  
谁都甘心流连塞上  
叫嚷  
朦着耳朵  
那里那天不再听到在呼号的人  
woo–ah woo–ah ah ah  
朦着眼睛  
再见往昔景仰的那样一道疤痕  
woo–ah woo–ah ah ah  
留在地壳头上  
无冕的身躯  
忘我的思想  
还有显赫的破墙  
谁也冲不开  
谁也抛不低  
谁要一生流离浪荡  
朦着耳朵  
那里那天不在听到像呼号的神  
woo–ah woo–ah ah ah  
朦着眼睛  
再见往昔景仰的那样一道疤痕  
woo–ah woo–ah ah ah  
留在地壳头上
{% endblockquote %}
