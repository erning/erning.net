---
layout: post
title: "Mac Tips 3 - Startup Options"
date: 2011-03-10
author: erning
comments: false
categories:
---

Startup keyboard shortcuts

  * `C` --- Start from a bootable disc (DVD, CD)
  * `T` --- Start in FireWire target disk mode
  * `N` --- Start from NetBoot server
  * `X` --- Force Mac OS X startup (if non-Mac OS X startup volumes are
    present)

  * `Shift` --- Boot in Safe Mode. Effectively the same as passing “-x”
    in Kernel Flags, and causes most caches to be ignored by the booter.

  * `Command-S` --- Boot in Single User Mode. Effectively the same as
    passing "-s" in Kernel Flags, and causes the system to boot to an
    interactive shell with no system services started. 

  * `Command-V` --- Boot in Verbose Mode. Effectively the same as passing
    "-v" in Kernel Flags, and causes the system to boot to verbose text
    logging before starting the graphical user interface.

  * `3` `2` --- Boot with the 32-bit kernel. Effectively the same as
    passing “arch=i386″ in Kernel Flags, and causes the system to prefer
    the 32-bit kernel on systems that would otherwise boot the 64-bit
    kernel.

  * `6` `4` --- Boot with the 64-bit kernel (if supported on this system).
    Effectively the same as passing “arch=x86_64″ in Kernel Flags, and
    causes the system to prefer the 64-bit kernel on systems that would
    otherwise boot the 32-bit kernel. If the 64-bit kernel is not supported,
    the option is ignored.


Hold down the **mouse’s primary key** during startup. On a two- or three-button mouse, the primary key is usually the left button. This shortcut will eject a CD or DVD from the optical drive.

Hold `Command` + `Option` + `p` + `r` during startup. This zaps the PRAM (Parameter RAM), an option that long-time Mac users will remember. Press and hold the key combination until you hear the second set of chimes. Zapping the PRAM returns it to its default configuration for display and video settings, time and date settings, speaker volume, and DVD region settings.

* [/Library/Preferences/SystemConfiguration/com.apple.Boot.plist][1]
* [Mac OS X keyboard shortcuts][2]
* [Mac OS X Startup Keyboard Shortcuts][3]

  [1]: http://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man5/com.apple.Boot.plist.5.html
  [2]: http://support.apple.com/kb/ht1343
  [3]: http://macs.about.com/od/macoperatingsystems/qt/osxstartupkeys.htm
