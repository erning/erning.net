---
layout: post
title: "Firefox Configuration"
date: 2005-11-08
author: erning
comments: false
categories:
---

**Generated:** Tue Nov 08 2005 20:16:10 GMT+0800 (CST)  
**User Agent:** Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20050929 Firefox/1.0.7  
**Build ID:** 2005092922

**Enabled Extensions:** (16)

  * Adblock 0.5.2.039
  * Add N Edit Cookies 0.2.1.0
  * Bookmarks Synchronizer 1.0.1
  * ColorZilla 0.8.2
  * CuteMenus – Crystal SVG 0.9.9.4
  * FlashGot 0.5.9.97
  * Greasemonkey 0.5.3
  * Header Monitor 0.3.2
  * Html Validator 0.7.6
  * Live HTTP Headers 0.10
  * MeasureIt 0.3.3
  * MR Tech About:About 2.0
  * MR Tech Local Install 4.0
  * Tabbrowser Preferences 1.2.8.7
  * View Rendered Source Chart 1.2.03
  * Web Developer 0.9.4

**Installed Themes:** (1)

  * none

**Installed Plugins:** (5)

  * Default Plugin
  * Java(TM) Plug-in Blackdown-1.4.2-02
  * QuickTime Plug-in 6.0, Windows Media Player Plugin are supported by mplayerplug-in
  * Scalable Vector Graphics
  * Shockwave Flash
