---
layout: post
title: "VIM Tips 2 - Viewports"
date: 2010-11-25
author: erning
comments: false
categories:
---

* `:sp` or `:split` 拆成上下两个显示区域
* `:vsp` or `:vsplit` 拆成左右两个显示区域

* `CTRL-w` `CTRL-w` 在显示区域间切换
* `CTRL-w` `j` 移到下面一个显示区域
* `CTRL-w` `k` 移到上面一个显示区域
* `CTRL-w` `h` 移到左面一个显示区域
* `CTRL-w` `l` 移到右面一个显示区域
* `CTRL-w` `=` 平分各个显示区域
* `CTRL-w` `-` 减小一行当前显示区域
* `CTRL-w` `+` 增加一行当前显示区域
* `CTRL-w` `q` 关闭当前显示区域
* `CTRL-w` `r` 向右(下)滚动显示区域
* `CTRL-w` `R` 向左(上)滚动显示区域

