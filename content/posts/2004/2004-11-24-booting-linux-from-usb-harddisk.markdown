---
layout: post
title: "Booting Linux from USB Harddisk"
date: 2004-11-24
author: erning
comments: false
categories:
---

闲暇的时候做了一个可以从USB硬盘启动的Linux(gentoo)，包括gcc, X, gnome，mozilla等。在我的笔记本(Dell 8600)上运行的很好，usb2加上1G的内存做cache速度够快了。

诀窍是安装好后修改initrd文件，加个命令行输入的等待，系统启动的时候给个机会拔插一下USB硬盘，等USB硬盘初始化好了之后再继续。

还一个是修改一下hotplug的stop脚本，别自动stop了USB的硬盘，否则shutdown的时候会halt。

为什么我不直接在dell 8600上安装linux呢？虽然大多数驱动程序，比如显卡、网卡、WiFi、声卡这些，但还一些特殊的驱动，像CDMA、GPRS、蓝牙这些我还没搞定。所以机器上的硬盘还是用Windows XP。

还有这个硬盘我可以拿到其他支持USB启动的机器上玩;-)

