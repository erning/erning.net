---
layout: post
title: "KDE on Cygwin"
date: 2003-07-19
author: erning
comments: false
categories:
---

I installed the KDE on Cygwin today. It looks good. But it works slowly and costs too many memory on my PC (PIII-800 512M).

It should be better if KDevelop has been ported to Cygwin.  
[![KDE on Cygwin][1]][2]

  * [Cygwin][3]
  * [Cygwin/XFree86][4]
  * [KDE on Cygwin][5]

  [1]: https://live.staticflickr.com/14/19430688_7a47620079.jpg
  [2]: http://www.flickr.com/photos/zendragon/19430688/
  [3]: http://www.cygwin.com/
  [4]: http://cygwin.com/xfree/
  [5]: http://kde-cygwin.sourceforge.net/
