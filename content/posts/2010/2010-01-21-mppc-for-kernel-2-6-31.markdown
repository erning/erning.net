---
layout: post
title: "mppc for kernel 2.6.31"
date: 2010-01-21
author: erning
comments: false
categories:
---

之前我搞了一台[华硕的小机器][1]做穿墙路由器，操作系统用的是[Gentoo Linux][2]。

SSH的socks5钻墙是好了，但我现在还想要开VPN到墙外，还有VPN到公司里，这样在家里用起来就更方便了。

有些[PPTP][3]的VPN需要[mppe][4],[mppc][5]。而我用的Gentoo不像Ubuntu，原装的内核不带mppe-mppc模块，得自己打[补丁][6]。可是这补丁只到2.6.13，之后的没有了。之前每次内核升级都遇到同样麻烦:|

不过最终还是google到了[devil-linux][7]项目里现成的[2.6.31补丁][8]。

    cd /usr/src/patches
    wget http://ftp.devil-linux.org/pub/devel/sources/1.4/linux-2.6.31-mppe-mppc-1.3.patch.bz2
    bunzip2 linux-2.6.31-mppe-mppc-1.3.patch.bz2
    cd /usr/src/linux
    patch -p1 < /usr/src/patches/linux-2.6.31-mppe-mppc-1.3.patch
    make menuconfig

选上`CONFIG_PPP_MPPE_MPPC=m`

    Device Drivers  --->
    [*] Network device support  --->
    <M>   PPP (point-to-point protocol) support
    <M>     Microsoft PPP compression/encryption (MPPC/MPPE)

然后重新编译安装内核。这样就有了`ppp_mppe_mppc`模块。

最后emerge ppp的时候需要带上`USE="mppe-pmmc"`参数。我是把"net-dialup/ppp mppe-mppc"添加到"/etc/portage/package.use"文件里。

  [1]: http://www.asus.com.cn/product.aspx?P_ID=QUObl5lSRQQ3lSqJ
  [2]: http://www.gentoo.org/
  [3]: http://en.wikipedia.org/wiki/Point-to-point_tunneling_protocol
  [4]: http://en.wikipedia.org/wiki/Microsoft_Point-to-Point_Encryption
  [5]: http://en.wikipedia.org/wiki/Microsoft_Point-to-Point_Compression
  [6]: http://mppe-mppc.alphacron.de/
  [7]: http://www.devil-linux.org/
  [8]: http://ftp.devil-linux.org/pub/devel/sources/1.4/linux-2.6.31-mppe-mppc-1.3.patch.bz2
