---
layout: post
title: "APPLESOFT BASIC CODE SAMPLE"
date: 2010-06-04
author: erning
comments: false
categories:
---

```basic
1 GR  
2 FOR I=1 TO 15:COLOR=I:HLIN 14,16 AT 11:VLIN 12,13 AT 13:VLIN 12,13 AT 17:HLIN 14,16 AT 14:VLIN 15,16 AT 13:VLIN 15,16 AT 17:HLIN 14,16 AT 17:HLIN 20,22 AT 11:VLIN 12,13 AT 19:VLIN 12,15 AT 23:HLIN 20,23 AT 14:PLOT 22,16:HLIN 19,21 AT 17:HLIN 15,17 AT 20:PLOT 14,21:VLIN 22,25 AT 13:HLIN 13,17 AT 23:VLIN 24,25 AT 17:HLIN 14,16 AT 26:PLOT 21,21:PLOT 20,22:PLOT 19,23:HLIN 19,23 AT 24:VLIN 20,26 AT 22:NEXT I:GOTO 2
```

复制上面的代码，并粘贴到下面这个网站，然后点击”Run”按钮  
[http://www.calormen.com/applesoft/][1]

看看我在[VirtualII][2]上的[运行结果][3]

* Wikipedia: [http://en.wikipedia.org/wiki/Applesoft_BASIC][4]
* Reference: [http://www.landsnail.com/a2ref.htm][5]

 [1]: http://www.calormen.com/applesoft/
 [2]: http://www.xs4all.nl/~gp/VirtualII/
 [3]: http://www.flickr.com/photos/zendragon/4666724359/
 [4]: http://en.wikipedia.org/wiki/Applesoft_BASIC
 [5]: http://www.landsnail.com/a2ref.htm
