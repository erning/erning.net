---
layout: post
title: "Cool Pictures"
date: 2003-07-16
author: erning
comments: false
categories:
---

Here’are some pictures of my brother and me token at our computer room.

[![][1]][flickr-19428546]
[![][2]][flickr-19428545]  
[![][3]][flickr-19428548]
[![][4]][flickr-19428547]  


  [1]: https://live.staticflickr.com/16/19428546_7418012dd3_m.jpg
  [2]: https://live.staticflickr.com/13/19428545_c84c8c6e60_m.jpg
  [3]: https://live.staticflickr.com/15/19428548_f23fb3cf8f_m.jpg
  [4]: https://live.staticflickr.com/15/19428547_3c248c3320_m.jpg

  [flickr-19428546]: https://www.flickr.com/photos/zendragon/19428546/
  [flickr-19428545]: https://www.flickr.com/photos/zendragon/19428545/
  [flickr-19428548]: https://www.flickr.com/photos/zendragon/19428548/
  [flickr-19428547]: https://www.flickr.com/photos/zendragon/19428547/

