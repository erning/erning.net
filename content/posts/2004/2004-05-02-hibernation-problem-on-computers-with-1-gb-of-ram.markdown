---
layout: post
title: "Hibernation Problem on Computers with 1 GB of RAM"
date: 2004-05-02
author: erning
comments: false
categories:
---

You may also unable to hibernate the laptop if it has 1 GB of RAM or more. I got the problem. Spent a whole afterroom googling the Internet I found the resolution finally.

  * [Microsoft Knowledge Base Article – 330909][1]
  * [Windows XP Patch][2]

 [1]: http://support.microsoft.com/?kbid=330909
 [2]: http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=4cbc68d2-09e1-4511-af14-03f357180135

