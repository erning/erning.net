---
layout: post
title: "动态域名解析服务"
date: 2010-01-25
author: erning
comments: false
categories:
---

有自己的VPS，很多服务都可以自己来做了，例如我自己安装一个VPN用来穿墙。今天用[PowerDNS][1]做了一个动态域名解析服务，替换过去[老旧的方法][2]。现在不仅可以自己用，还可以提供给其他有动态域名解析需求的同学。

更新域名的API很简单，一个HTTP请求即可。API地址是: `http://dynamic.wacao.com/api/plain.php`，参数为，

  * `host` – 主机名，FQDN为 `${host}.dynamic.wacao.com`
  * `time` – unixtime形式的请求时间，时间误差在正负5分钟内有效
  * `sign` – 签名，格式为 `sha1(${host}${time}${shared_key})`
  * `ip` – 可选。如果未提供IP地址则采用客户端请求的来源地址

其中**host**和**shared_key**需要我来提供。这里提供一个的host和shared_key给大家测试。`testing`, `d035cdac09dd866ed8f4a244567c4daf`。域名为testing.dynamic.wacao.com.。

由于仅是一个HTTP请求，客户端很容易用各种语言实现。我自己用的是一个简单的shell脚本。

```bash
#!/bin/bash
 
function usage {
    echo "Usage: $0 {host} {shared_key}"
}
 
if test -z $1 || test -z $2
then
    usage
    exit 1
fi
 
HOST=$1
SHARED_KEY=$2
ENDPOINT="http://dynamic.wacao.com/api/plain.php"
 
TIME=`date +%s`
SIGN=`echo -n "${HOST}${TIME}${SHARED_KEY}" | sha1sum -t | awk '{print $1}'`
URL="${ENDPOINT}?host=${HOST}&time=${TIME}&sign=${SIGN}"
curl "$URL"
```

执行后立即能够解析，TTL为5秒。


    $ update-dynamic-dns.sh testing d035cdac09dd866ed8f4a244567c4daf
    ok. 58.33.69.77
    $ host testing.dynamic.wacao.com.
    testing.dynamic.wacao.com has address 58.33.69.77

我将这个脚本加到crontab里，每5分钟更新一次。

    */5 * * * * /usr/local/sbin/update-dynamic-dns.sh ${HOST} ${SHARED_KEY}

如果觉得testing.dynamic.wacao.com这样的域名不好看，而且你有自己的域名，可以想我一样，给自己的域名加一个CNAME。例如我把alcohol.swanpan.com做了一个CNAME指到alcohol.dynamic.wacao.com


    ;; ANSWER SECTION:
    alcohol.swanpan.com.    86400   IN  CNAME   alcohol.dynamic.wacao.com.
    alcohol.dynamic.wacao.com. 5    IN  A   58.33.69.77

这样我在各个地方都能用alcohol.swanpan.com这个域名访问家里的机器，方便[用它穿墙][3]。

想试试的朋友告诉我你要的host，我给你发送shared_key。

  [1]: http://www.powerdns.com/
  [2]: /blog/2006/04/02/dynamic-dns-diy/
  [3]: /blog/2010/01/05/my-linux-gateway/
