---
layout: post
title: "在Fedora的英文界面下使用中文"
date: 2004-01-05T13:00:00
author: erning
comments: false
categories:
---

Fedora的英文界面已经非常漂亮了，可是中文界面还是比较丑陋。下面是我怎样在Fedora的英文界面(en_US.UTF-8)下安装中文支持。

## Fedora安装

安装的时候Locale仅选择English(USA) 就可以了。中文那些只是安装了些字体和输入法，而那些又都是不需要的。

我还选择安装了Gnome, Development, Kernel Development, Gnome Development和X Development。

## 字体安装

将Microsoft Windows下的相应TrueType拷贝到Fedora相应目录。我是放在/usr/share/fonts/windows目录下。

然后以root执行下面的命令

    cd /usr/share/fonts/windows
    ttmkfdir .
    cp fonts.scale fonts.dir
    chkfontpath --add /usr/share/fonts/windows

以下是我安装的字体文件

  * (Arial) - arial.ttf arialbd.ttf arialbi.ttf ariali.ttf
  * (Courier New) - cour.ttf courbi.ttf courbd.ttf couri.ttf
  * (Tahoma) - tahoma.ttf tahomabd.ttf
  * (Verdana) - verdana.ttf verdanab.ttf verdanai.ttf verdanaz.ttf
  * (GB) - simsun.ttc
  * (BIG5) - mingliu.ttc
  * (KR) - batang.ttc gulim.ttc
  * (JP) - msgothic.ttc msmincho.ttc

## 输入法安装

输入法我选择[SCIM][1]，这个输入法支持在英文Locale下输入中文。下载地址为[http://www.turbolinux.com.cn/~suzhe/scim/][2]。找需要的RPM包下载并安装就可以了。我只安装了下面两个RPM。

    rpm -hUv scim-0.8.2-1.i586.rpm
    rpm -hUv scim-chinese-0.2.6-1.i586.rpm

接着要修改一下/etc/X11/xinit/xinitrc.d/xinput文件，让SCIM在X启动的时候也启动。下面是patch文件

    115c115,118
    < XIM="none"
    ---
    > # XIM="none"
    > if [ -e /usr/bin/scim ]; then
    > XIM="SCIM"
    > fi
    174a178,180
    > SCIM)
    > XIM_PROGRAM=scim
    > XIM_ARGS="-d" ;;

这个patch没有修改中文Locale下的输入方式，如果需要可以自己修改。找找该文件中的zh_CN*)、zh_TW*)的部分，然后修改一下。

接着还要修改一下/etc/gtk-2.0/gtk.immodules文件，让gtk的应用程序使用X的输入法。patch文件如下

    37c37
    < "xim" "X Input Method" "gtk20" "/usr/share/locale" "ko:ja:zh"
    ---
    > "xim" "X Input Method" "gtk20" "/usr/share/locale" "en:ko:ja:zh"

我还将该文件中的其他不需要的输入法都注释。

### 搞定

重新启动一下X，看看效果。

照此处理之后，不用再做其他设置，中文就可以显示得很漂亮，中文输入也可以使用了。当然还没有100%弄好，比如中文的粗体和斜体还有些问题。xmms，Acrobat Redaer等中文还需要另外设置。但这已经是一个可以工作的环境了。
