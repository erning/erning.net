---
layout: post
title: "Develop PHP with Eclipse"
date: 2003-07-25
author: erning
comments: false
categories:
---

There’re many useful plug-ins for Eclipse. You can find them easily by a search engine.
Here’re some screenshots of a PHP plug-in for Eclipse I’m using.

![][1]  
Editing the php files

![][2]  
Running as script

![][3]  
Running in the internal webserver

![][4]  
Debugging

![][5]  
Working with MySQL Control Center

  [1]: http://www.dragonsoft.net/articles/eclipse-php/php-edit-s.png
  [2]: http://www.dragonsoft.net/articles/eclipse-php/php-run-script-s.png
  [3]: http://www.dragonsoft.net/articles/eclipse-php/php-run-web-s.png
  [4]: http://www.dragonsoft.net/articles/eclipse-php/php-debug.png
  [5]: http://www.dragonsoft.net/articles/eclipse-php/php-mysql-s.png

