---
layout: post
title: "RewriteModule for ASP.NET 2.0"
date: 2005-12-12
author: erning
comments: false
categories:
---

[mod_rewrite][1] is one of the most useful module for [Apache][2].

But what to do if I’m on a Windows IIS server? I only googled a few commercial ISAPI modules ([ISAPI ReWrite][3], [IIS Rewrite][4]). This is the most painful on Windows platform.

OK. Since I only need a few feature of the rewrite currently. I decided to do it myself. An early version is available from [CVS repository][5] – anoncvs without password.

Yeah. The RewriteCond feature haven’t been implemented. Sorry It’s only one afternoon work and I’m still a newbie on ASP.NET platform.

Here’s the sample configuration

```xml
<configuration>
    <configsections>
<section
            name="rewriteSection"
            type="Dragonsoft.RewriteConfigurationSection, Dragonsoft.RewriteModule" />
    </configsections>

    <system .web>
        <httpmodules>
            <add name="rewrite" type="Dragonsoft.RewriteModule" />
        </httpmodules>
    </system>

    <rewritesection>
        <![CDATA[
        RewriteRule ^~/news/([^/]+)/([^/]+)\.html$ ~/news.aspx?id=$1&#038;name=$2 [L]
        RewriteRule \fake\(.*).html$ \real\$1.aspx [NC,L]
        ]]&gt;
    </rewritesection>
</configuration>
```

–update: [URL Rewrite Filter for IIS][6] is open source. I haven’t tested it.

  [1]: http://httpd.apache.org/docs/2.2/mod/mod_rewrite.html
  [2]: http://httpd.apache.org/
  [3]: http://www.isapirewrite.com/
  [4]: http://www.qwerksoft.com/products/iisrewrite/
  [5]: http://cvs.dragonsoft.net/horde/chora/cvs.php/rewrite-module/RewriteModule
  [6]: http://www.iismods.com/url-rewrite/index.htm
