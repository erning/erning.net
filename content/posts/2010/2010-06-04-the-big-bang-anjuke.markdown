---
layout: post
title: "The Big Bang Anjuke"
date: 2010-06-04T12:00:00
author: erning
comments: false
categories:
---

[![Code Swarm][2]][1]

上面这张图就是安居客在Subversion源代码库里的代码大爆炸。

接下来的视频可以看到从2007年的一个工程师到现在三十来个工程师的爆炸过程，挺有趣的。

{% youtube VDCmIRW9rlE %} 

--- [CodeSwarm][3]

  [1]: http://www.flickr.com/photos/zendragon/4666092652/
  [2]: http://farm5.static.flickr.com/4026/4666092652_9c563c05ff.jpg
  [3]: http://vis.cs.ucdavis.edu/~ogawa/codeswarm/
