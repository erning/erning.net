---
layout: post
title: "Dial-up GPRS over bluetooth with iPAQ 3870"
date: 2003-08-07
author: erning
comments: false
categories:
---

Installed familiar 0.7 stable GPE Linux on my brother’s iPAQ 3870.
It looks much better than before.
Finally, I configurated the dial-up GPRS over bluetooth successfully. (I’ve broken the bluetooth’s chip of my 3870. And it is unable to be fixed :-< .)

Resources,

  * [HOW-TO][1]
  * [Handhelds.org][2]

  [1]: http://www.eit.se/hb/misc/text/ipaq_gprs_bluetooth.txt
  [2]: http://www.handhelds.org/
